import datetime as dt
import math
import sys

THEMES = {
    'gitlab': [
        (227, 227, 227),
        (172, 213, 242),
        (127, 168, 201),
        (82, 123, 160),
        (37, 78, 119),
    ],
    'github': [
        (228, 228, 228),
        (198, 228, 139),
        (123, 201, 111),
        (35, 154, 59),
        (25, 97, 39),
    ],
}


def func(x):
    return x + 1


def _normalize(v, min=0, max=255):
    if v < min:
        return 0
    if v > max:
        return max
    return math.ceil(4 * (v - min) / (max - min))


def _rgb(text, r=0, g=0, b=0):
    assert 0 <= r and r <= 255, "Invalid red value: {}".format(r)
    assert 0 <= g and g <= 255, "Invalid green value: {}".format(g)
    assert 0 <= b and b <= 255, "Invalid blue value: {}".format(b)
    return "\x1b[38;2;{r};{g};{b}m{text}\x1b[0m".format(
        r=r, g=g, b=b, text=text
    )


def _getlabels(start, end, format="%b"):
    """ Generate a label for each week where given format changes. """
    col = 0
    current = (start - dt.timedelta(days=1)).strftime(format)
    d = start
    while d < end:
        if d.weekday() == 0:
            col += 1
        label = d.strftime(format)
        if label != current:
            current = label
            yield (col, label)
        d += dt.timedelta(days=1)


def _value2str(v, theme, symbol="⯀", min=0, max=255):
    """ Turn a (numerical) value into a string with a color representing the
    value """
    if v is None:
        return " " * len(symbol)
    v = _normalize(v, min, max)
    return _rgb(symbol, *THEMES[theme][v])


def _labels2str(labels, colw=1, maxw=80):
    string = ""
    for position, label in labels:
        if colw * position + len(label) >= maxw:
            break
        while len(string) < position * colw:
            string += ' '
        string += label
    return string


def printcal(
    vals,
    start_date,
    theme,
    symbol="⯀",
    mlabels=True,
    ylabels=True,
    width=80,
    minval=None,
    maxval=None,
    truncate='left',
):
    assert (
        isinstance(symbol, str) and len(symbol) > 0
    ), "symbol must be a non empty string"
    assert theme in [
        'github',
        'gitlab',
    ], "'theme' should be one of 'github' or 'gitlab'"
    assert truncate in [
        'right',
        'left',
    ], "truncate should be one of 'left' or 'right'"
    assert isinstance(start_date, dt.date)

    minval = min(vals) if minval is None else minval
    maxval = max(vals) if maxval is None else maxval

    # Pad values with "None" so that it starts with a monday
    vals = [None] * start_date.weekday() + list(vals)

    if truncate == 'right':
        vals = vals[: int(width / len(symbol)) * 7]
    else:
        skip = (int((len(vals) + 6) / 7) - int(width / len(symbol))) * 7
        if skip > 0:
            vals = vals[skip:]
            start_date += dt.timedelta(days=skip)

    end_date = start_date + dt.timedelta(days=len(vals))
    if ylabels:
        lbs = _getlabels(start_date, end_date, "%Y")
        print(_labels2str(lbs, colw=len(symbol), maxw=width))
    if mlabels:
        lbs = _getlabels(start_date, end_date, "%b")
        print(_labels2str(lbs, colw=len(symbol), maxw=width))

    for i in range(0, 7):
        for v in vals[i::7]:
            sys.stdout.write(
                _value2str(
                    v, symbol=symbol, theme=theme, min=minval, max=maxval
                )
            )
        sys.stdout.write('\n')

    legend = ''
    legend += 'Less '
    for color in THEMES[theme]:
        legend += _rgb(symbol, *color)
    legend += 'More'
    sys.stdout.write('{}{}\n'.format(' ' * (width - 20), legend))
