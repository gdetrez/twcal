from datetime import date

from calviz import _getlabels, _labels2str, _normalize, _rgb, _value2str


def test_normalize():
    assert _normalize(5, 1, 10) == 2


def test_normalize_below_min():
    """ Normalizing a value below the minimum should return 0 """
    assert _normalize(1, 2, 3) == 0


def test_normalize_above_max():
    """ Normalizing a value above the maximum should return the maximum """
    assert _normalize(4, 2, 3) == 3


def test_rgb():
    """ The rgb function returns the given text surrounded by  ISO-8613-3
    escape codes to set the foreground color to the given rgb values """
    assert _rgb("foo", 16, 32, 64) == "\x1b[38;2;16;32;64mfoo\x1b[0m"


def test_getlabels():
    """ Starting in the 15 of november 2016, the next label is December on
    column 2 as this is where dec 01 is. """
    assert list(_getlabels(date(2016, 11, 15), date(2017, 1, 15))) == [
        (2, "Dec"),
        (6, "Jan"),
    ]


def test_getlabels_year():
    """ Starting in the 15 of november 2016, the next label is 2017 on
    column 6 as this is where january 01 is. """
    assert list(_getlabels(date(2016, 11, 15), date(2017, 1, 15), "%Y")) == [
        (6, "2017")
    ]


def test_value2str():
    """ A turn a value into a string containing a square with color
    corresponding to the value """
    assert _value2str(123, 'github') == _rgb("⯀", 123, 201, 111)


def test_value2str_custom_symbol():
    """ You can give a custom symbol to the function """
    assert _value2str(123, 'github', symbol="☀") == _rgb("☀", 123, 201, 111)


def test_value2str_none():
    """ If the value is None, print spaces for the length of the symbol """
    assert _value2str(None, 'github', symbol="...") == "   "


def test_value2str_normalize():
    """ If the value is normalized according to the given values for min and
    max """
    assert _value2str(127, 'github', max=127) == _rgb("⯀", 25, 97, 39)
    assert _value2str(127, 'github', min=127) == _rgb("⯀", 228, 228, 228)


def test_labels2str():
    """ Print a list of labels at their given column """
    assert "  foo   bar" == _labels2str([(2, "foo"), (8, "bar")])


def test_labels2str_colw():
    """ it allows the caller to define the width of a column """
    assert "    foo         bar" == _labels2str(
        [(2, "foo"), (8, "bar")], colw=2
    )


def test_labels2str_maxw():
    """ the resulting string is truncated at maxw """
    assert "  foo" == _labels2str([(2, "foo"), (8, "bar")], maxw=10)
