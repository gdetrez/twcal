from setuptools import setup

setup(
    name='twcal',
    version='1.0.1',
    description='Taskwarrior log visualizer',
    author='Grégoire Détrez',
    author_email='gregoire@fripost.org',
    url='https://gitlab.com/gdetrez/twcal',
    py_modules=['calviz'],
    scripts=['twcal'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
