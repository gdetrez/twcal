# twcal

`twcal` displays a log of your completed taskwarrior tasks as an horizontal
calendar, like the contribution graph on github:

![screenshot](https://gitlab.com/gdetrez/twcal/raw/master/screenshot.png)


## Usage

```sh
twcal
```


## Contributing

- Issue Tracker: https://gitlab.com/gdetrez/twcal/issues
- Source Code: https://gitlab.com/gdetrez/twcal

To contribute, please open a merge requests on gitlab.

## Installation

### Requirements

`twcal` requires python 3 and also taswarrior to be in your `$PATH`. In
addition, it requires your terminal to support ISO-8613-3 24-bit foreground and
background color setting.

### Installation

Install twcal from pypi with
```sh
pip install twcal
```

## Credits

Grégoire Détrez

## License

This code is licensed under the GNU GPLv3 license.
